from glob import glob, iglob
from os import sep
import re


import requests


url = 'https://gitlab.com/yash2127/android-gitlab-ci/-/archive/master/android-gitlab-ci-master.zip'
r = requests.get(url, allow_redirects=True)
open('data.zip', 'wb').write(r.content)


# importing required modules
from zipfile import ZipFile
  
# specifying the zip file name
file_name = "data.zip"
  
# opening the zip file in READ mode
with ZipFile(file_name, 'r') as zip:
    # printing all the contents of the zip file
    zip.printdir()
  
    # extracting all the files
    print('Extracting all the files now...')
    zip.extractall()
    print('Done!')

# path = "./data"
# text_files = glob(path+"/**/*.md", recursive = True)

# urls=[]

# for j in range(0,len(text_files)):
#     with open(text_files[j],encoding="utf8") as file:
#         for line in file:
#             url = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', line)
#             if(len(url)>0):
#                 for i in range(0,len(url)):
#                     urls.append(url[i])

# print(*urls,sep="\n")


# import os
# path =r'.\data'
# list_of_files = []
# for root, dirs, files in os.walk(path):
# 	for file in files:
# 		list_of_files.append(os.path.join(root,file))

# urls=[]
# for name in list_of_files:
#     if(".md" in name):
#         # urls.append(name)
#         with open(name,encoding="utf8") as file:
#             for line in file:
#                 url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', line)
#                 # urls.append(url)
#                 if(len(url)>0):
#                     for i in range(0,len(url)):
#                         urls.append(url[i])
# print(*urls,sep="\n")